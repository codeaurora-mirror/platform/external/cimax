/**************************************************************************//**
Copyright (c) 2012, The Linux Foundation. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.
    * Neither the name of The Linux Foundation, nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
******************************************************************************/

/**************************************************************************//**

                            INCLUDE FILES FOR MODULE

******************************************************************************/
#include <pthread.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include "os.h"

/**************************************************************************//**

                             FUNCTION DEFINITIONS

******************************************************************************/

/**
FUNCTION: OS_MutexCreate

@brief
  Create and initialize a mutex.

@return
  Pointer to initialized mutex.

@note
  None
@callgraph
@callergraph
******************************************************************************/
osMutex_t *OS_MutexCreate(void)
{
  int ret = 0;
  pthread_mutex_t *pMutex = (pthread_mutex_t *)malloc(sizeof(pthread_mutex_t));

  if (NULL == pMutex)
  {
    return NULL;
  }

  /* initialize mutex with default attributes */
  ret = pthread_mutex_init(pMutex, NULL);
  if (0 != ret)
  {
    free(pMutex);
    return NULL;
  }

  return pMutex;
}

/**
FUNCTION: OS_MutexDelete

@brief
  Un-initialize and delete a mutex.

@return
  None.

@note
  None
@callgraph
@callergraph
******************************************************************************/
void OS_MutexDelete(osMutex_t *pMutex)
{
  if (NULL != pMutex)
  {
    pthread_mutex_destroy(pMutex);
    free(pMutex);
  }
}

/**
FUNCTION: OS_MutexLock

@brief
  Lock a mutex.

@return
  None.

@note
  None
@callgraph
@callergraph
******************************************************************************/
void OS_MutexLock(osMutex_t *pMutex)
{
  if (NULL != pMutex)
  {
    pthread_mutex_lock(pMutex);
  }
}

/**
FUNCTION: OS_MutexUnlock

@brief
  Unlock a mutex.

@return
  None.

@note
  None
@callgraph
@callergraph
******************************************************************************/
void OS_MutexUnlock(osMutex_t* pMutex)
{
  if (NULL != pMutex)
  {
    pthread_mutex_unlock(pMutex);
  }
}

/**
FUNCTION: OS_Sleep

@brief
  Sleep for a given amount of time (ms).

@return
  None.

@note
  None
@callgraph
@callergraph
******************************************************************************/
void OS_Sleep(uint32 ms)
{
  usleep(ms * 1000);
}

/**
FUNCTION: OS_Memcmp

@brief
  Compare a specified number of bytes in two memory locations.

@return
  < 0: dst is smaller than src.
  0:   dst and src are equal.
  > 0: dst is bigger than src.

@note
  None
@callgraph
@callergraph
******************************************************************************/
int OS_Memcmp(const void *dst, const void *src, unsigned int size)
{
  return memcmp(dst, src, size);
}

/**
FUNCTION: OS_FileOpen

@brief
  Open a file in a given mode (read/write etc.).

@return
  Pointer to file.

@note
  None
@callgraph
@callergraph
******************************************************************************/
osFile_t *OS_FileOpen(const char *pcFilename, const char *pcMode)
{
  return fopen(pcFilename, pcMode);
}

/**
FUNCTION: OS_FileClose

@brief
  Close a file.

@return
  0 on success, EOF otherwise.

@note
  None
@callgraph
@callergraph
******************************************************************************/
int32 OS_FileClose(osFile_t* pFile)
{
  return fclose(pFile);
}

/**
FUNCTION: OS_FileRead

@brief
  Read a specific number of bytes from a file.

@return
  Number of bytes successfully read.

@note
  None
@callgraph
@callergraph
******************************************************************************/
int32 OS_FileRead(void* pData, int32 size, osFile_t* pFile)
{
  return fread(pData, 1, size, pFile);
}

/**
FUNCTION: OS_FileSeek

@brief
  Move file position to a specific location in the file.

@return
  0 on success, nonzero value otherwise.

@note
  None
@callgraph
@callergraph
******************************************************************************/
int32 OS_FileSeek(osFile_t* pFile, int32 offset, int32 origin)
{
  return fseek(pFile, offset, origin);
}

/**
FUNCTION: OS_ThreadCreate

@brief
  Create a thread.

@return
  Pointer to thread.

@note
  None
@callgraph
@callergraph
******************************************************************************/
osThread_t *OS_ThreadCreate(void* (*pstFunction)(void*), void* pParam,
                              int32 stackSize)
{
  int ret = 0;
  pthread_t *pThread = (pthread_t *)malloc(sizeof(pthread_t));

  if (NULL == pThread)
  {
    return NULL;
  }

  /* create thread with default attributes. */
  /* we do not set stack size specifically. */
  ret = pthread_create(pThread, NULL, pstFunction, pParam);
  if (0 != ret)
  {
    free(pThread);
    return NULL;
  }

  return pThread;
}

/**
FUNCTION: OS_ThreadDelete

@brief
  Delete a thread.

@return
  Cancel a thread.

@note
  0 on success, nonzero value otherwise.
@callgraph
@callergraph
******************************************************************************/
int32 OS_ThreadDelete(osThread_t *pThread)
{
  int ret = -1;

  if (NULL != pThread)
  {
    ret = pthread_join(*(pthread_t *)pThread, NULL);
    free(pThread);
  }

  return ret;
}


